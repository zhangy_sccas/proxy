package main

import (
	"log"
	"fmt"
	"net"
	"sync"
	"flag"
)

func change(rConn net.Conn, wConn net.Conn) {
	buf := make([]byte, 1024)
	for {
		n, err := rConn.Read(buf)
		if err != nil {
			log.Fatal(err)
		}
		wConn.Write(buf[:n])
	}
}

var remoteAddr = flag.String("raddress", "127.0.0.1", "ip address of remote server")
var remotePort = flag.Int("rport", 80, "port of remote server")

func main() {
	flag.Parse()
	var wg sync.WaitGroup
	//clientChan := make(chan net.Conn)
	ln, err := net.Listen("tcp", ":8888")
	defer ln.Close()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Proxy Server Start.")
	clientConn, err := ln.Accept()
	defer clientConn.Close()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Proxy client From:%s\n", clientConn.RemoteAddr())

	addr := fmt.Sprintf("%s:%d", *remoteAddr, *remotePort)
	servConn, err := net.Dial("tcp", addr)
	defer servConn.Close()
	if err != nil {
		log.Fatal(err)
	}

	wg.Add(1)
	go change(clientConn, servConn)
	wg.Add(1)
	go change(servConn, clientConn)
	wg.Wait()
}
