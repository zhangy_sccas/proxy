package main

import (
	"net"
	"log"
	"fmt"
	"time"
)

func read(conn net.Conn) {
	buf := make([]byte, 1024)
	for {
		n, err := conn.Read(buf)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf(string(buf[:n]))
	}

}

func write(conn net.Conn) {
	var msg string
	var id = 0
	for {
		id++
		msg = fmt.Sprintf("id:%d,msg:%s\n", id, "hello")
		_, err := conn.Write([]byte(msg))
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(5 * time.Second)
	}

}

func echo(conn net.Conn) {
	go read(conn)
	go write(conn)
}
func main() {
	ln, err := net.Listen("tcp", ":8080")
	defer ln.Close()
	fmt.Println("Echo Server Start.")
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("%s connect fail.\n", conn.RemoteAddr())
		}
		fmt.Printf("From:%s\n", conn.RemoteAddr())
		go echo(conn)
	}
}
